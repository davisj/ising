from ising.graph import SpinGraph, toroidal_lattice, hexagonal_lattice
from ising.metropolis import metropolis_step, metropolis_iterate

#import matplotlib.pyplot as plt
import scipy as sp
import pygame
import sys
from pygame.locals import *

pygame.init()
FPS = 80
clock = pygame.time.Clock()

#g = toroidal_lattice(256, 256)
g = hexagonal_lattice(128, 128)
for i in range(1, 257):
    if sp.random.random() >= 0.5:
        g.vertices[i] = -1
    else:
        g.vertices[i] = 1

window = pygame.display.set_mode((512, 512), 0, 32)
window.fill((0,0,0))

RED = (255, 128, 0)
BLUE = (0, 64, 255)

def draw(g):
    for i in range(128):
        for j in range(128):
            offset = 2 if j % 2 else 0
            spin = g.vertices[i * 128 + j + 1]
            color = RED if spin > 0 else BLUE
            pygame.draw.circle(window, color, (2 + offset + i * 4, 2 + j * 4), 2)


pygame.image.save(window, "ising-hexagonal.png")
while True:
    metropolis_iterate(64*64, g, 0.0, 1.0, 1.0)
    window.fill((0,0,0))
    draw(g)
    pygame.display.flip()
    pygame.display.update()
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    clock.tick(FPS)
#metropolis_step(g, 0.0, 1.0, 1.0)

#plot = g.vertices[1:].reshape(256,256)
#plt.imshow(plot)

#plt.figure()

#metropolis_iterate(65536*4096, g, 0.0, 1.0, 1.0)
#plt.imshow(g.vertices[1:].reshape(256,256))

#plt.show()

