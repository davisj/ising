from ising.graph import SpinGraph, toroidal_lattice
from ising.metropolis import metropolis_iterate

import matplotlib as mpl
mpl.use('pgf')
mpl.rcParams.update({
    'figure.figsize': (4, 3),
    'figure.autolayout': True,
    'font.size': 8,
    'pgf.texsystem': 'pdflatex',
    'font.family': 'serif',
    'font.serif': [],
    'text.usetex': True,
    'pgf.rcfonts': False,
    'pgf.preamble': [
        r'\usepackage{Baskervaldx}',
        r'\usepackage[Baskervaldx]{newtxmath}',
    ]
})
import matplotlib.pyplot as plt

w, h = 512, 512

g = toroidal_lattice(w, h)
fig = plt.figure()
ax0 = fig.add_subplot(121)
ax1 = fig.add_subplot(122)

ax0.imshow(g.vertices[1:].reshape(w,h))
#plt.savefig("ising-initial.png", dpi=1000)
#plt.figure()
metropolis_iterate(512*512*100, g, 0.0, 1.0, 1.0)
#plt.imshow(g.vertices[1:].reshape(w,h))
#plt.savefig("ising-evolved.png", dpi=1000)
ax1.imshow(g.vertices[1:].reshape(w,h))
fig.savefig("ising.png", dpi=1000)
plt.show()

print("x")
