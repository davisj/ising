
from ising.graph import (
    SpinGraph,
    toroidal_lattice,
    toroidal_octagonal_lattice,
    hexagonal_lattice,
)
from ising.metropolis import metropolis_step, metropolis_iterate
from ising.utils import total_energy, total_magnetization

import matplotlib as mpl
mpl.use('pgf')
mpl.rcParams.update({
    'figure.figsize': (4, 3),
    'figure.autolayout': True,
    'font.size': 8,
    'pgf.texsystem': 'pdflatex',
    'font.family': 'serif',
    'font.serif': [],
    'text.usetex': True,
    'pgf.rcfonts': False,
    'pgf.preamble': [
        r'\usepackage{Baskervaldx}',
        r'\usepackage[Baskervaldx]{newtxmath}',
    ]
})

import matplotlib.pyplot as plt
import scipy as sp
import time
import numba as nb

w, h = 256, 256
#w, h = 16, 16
#N = 65536 * 100
#N = 65536 * 1024
N = 1024

gx = hexagonal_lattice(w, h)
go = toroidal_octagonal_lattice(w,h)

g = toroidal_lattice(w, h)

temps = sp.linspace(1, 6, 40)
magsx = []
magso = []
mags = []

mags_std = []
magsx_std = []
magso_std = []

energies = []
energiesx = []
energieso = []

energies_std = []
energiesx_std = []
energieso_std = []

start = time.clock()

@nb.njit
def find_magnetization_and_energy(g, T):
    mag = sp.empty(512)
    e = sp.empty(512)
    for i in range(512):
        metropolis_iterate(w*h, g, 0.0, 1.0, T)
        mag[i] = total_magnetization(g)
        e[i] = total_energy(g, 0.0, 1.0, T)
    return (
        e.mean() / (w*h), e.std() / (w*h),
        mag.mean() / (w * h), mag.std() / (w*h)
    )

for T in temps:
    print(f"Running for T={T}: ", flush=True, end='')
    g.vertices[1:] = 1
    gx.vertices[1:] = 1
    go.vertices[1:] = 1
    metropolis_iterate(N, g, 0.0, 1.0, T)
    metropolis_iterate(N, gx, 0.0, 1.0, T)
    metropolis_iterate(N, go, 0.0, 1.0, T)

    e, sigma_e, m, sigma_m = find_magnetization_and_energy(g, T)
    mags.append(m)
    mags_std.append(sigma_m)
    energies.append(e)
    energies_std.append(sigma_e)

    e, sigma_e, m, sigma_m = find_magnetization_and_energy(gx, T)
    magsx.append(m)
    magsx_std.append(sigma_m)
    energiesx.append(e)
    energiesx_std.append(sigma_e)

    e, sigma_e, m, sigma_m = find_magnetization_and_energy(go, T)
    magso.append(m)
    magso_std.append(sigma_m)
    energieso.append(e)
    energieso_std.append(sigma_e)
    print("done")

end = time.clock()

print(f"Simulation finished in {end - start} seconds")

plt.figure()
plt.plot(temps, mags, 'xk')
plt.xlabel("$T$")
plt.ylabel("$M$")
plt.title("Magnetization vs. Temperature (Rectangular Lattice)")
plt.savefig("magnetization-rect.pdf")

plt.figure()
plt.plot(temps, magsx, 'xk')
plt.xlabel("$T$")
plt.ylabel("$M$")
plt.title("Magnetization vs. Temperature (Triangular Lattice)")
plt.savefig("magnetization-tri.pdf")

plt.figure()
plt.plot(temps, magso, 'xk')
plt.xlabel("$T$")
plt.ylabel("$M$")
plt.title("Magnetization vs. Temperature (Octagonal Lattice)")
plt.savefig("magnetization-oct.pdf")

plt.figure()
plt.plot(temps, energies, 'xk')
plt.xlabel("$T$")
plt.ylabel("$E$")
plt.title("Energy vs. Temperature (Rectangular Lattice)")
plt.savefig("energy-rect.pdf")

plt.figure()
plt.plot(temps, energiesx, 'xk')
plt.xlabel("$T$")
plt.ylabel("$E$")
plt.title("Energy vs. Temperature (Triangular Lattice)")
plt.savefig("energy-tri.pdf")

plt.figure()
plt.plot(temps, energieso, 'xk')
plt.xlabel("$T$")
plt.ylabel("$E$")
plt.title("Energy vs. Temperature (Octagonal Lattice)")
plt.savefig("energy-oct.pdf")

cap = sp.array(energies_std) ** 2 / temps ** 2
capx = sp.array(energiesx_std) ** 2 / temps ** 2
capo = sp.array(energieso_std) ** 2 / temps ** 2

print(f"T_C (rect): {temps[sp.argmax(cap)]}")
print(f"T_C (tri): {temps[sp.argmax(capx)]}")
print(f"T_C (oct): {temps[sp.argmax(capo)]}")

plt.figure()
plt.plot(temps, cap, 'xk')
plt.ylabel("$C$")
plt.xlabel("$T$")
plt.title("Heat Capacity vs. Temperature (Rectangular Lattice)")
plt.savefig("heat-capacity-rect.pdf")

plt.figure()
plt.plot(temps, capx, 'xk')
plt.ylabel("$C$")
plt.xlabel("$T$")
plt.title("Heat Capacity vs. Temperature (Triangular Lattice)")
plt.savefig("heat-capacity-tri.pdf")

plt.figure()
plt.plot(temps, capo, 'xk')
plt.ylabel("$C$")
plt.xlabel("$T$")
plt.title("Heat Capacity vs. Temperature (Octagonal Lattice)")
plt.savefig("heat-capacity-oct.pdf")
