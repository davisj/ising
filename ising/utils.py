from ising.graph import SpinGraph

import numba as nb
import scipy as sp

@nb.njit(parallel=True)
def total_energy(graph, H, j, kt):
    e = 0.0
    for i in nb.prange(1, len(graph.vertices)):
        s = graph.vertices[i]
        for k in graph.neighbours(i):
            if i < k: # Make sure we only count each vertex once
                e += s * graph.vertices[k]
    e *= -j
    if H != 0.0:
        e += -H * total_magnetization(graph)
    return e


@nb.njit
def total_magnetization(graph):
    return sp.sum(graph.vertices[1:])

