import scipy as sp
import math
from numba import njit, jit
from math import exp

from .graph import SpinGraph


@njit
def metropolis_step(graph, H, j, kt):
    i = sp.random.randint(1, graph.vertices.shape[0])
    s = graph.vertices[i]
    de = 0.0
    for k in graph.neighbours(i):
        de += graph.vertices[k]
    de *= j
    de += H
    de *= 2 * s

    prob = math.exp(-de / kt)

    if de < 0 or sp.random.random() < prob:
        graph.flip(i)

@njit
def metropolis_iterate(n, graph, h, j, kt):
    for i in range(n):
        metropolis_step(graph, h, j, kt)


