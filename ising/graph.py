from numba import jitclass, int8, int64
import numpy as np
import scipy as sp


# Define the layout in memory of class Graph's members
graph_spec = [
    ('vertices', int8[:]),
    ('edges', int64[:,:]),
]


@jitclass(graph_spec)
class SpinGraph:
    def __init__(self, n, edges):
        # This is probably a premature optimization, but I'm implmenting this
        # two days before the deadline. Essentially, I've offset the indices of
        # the vertices of the graph by one, in order to have one element in the
        # array that is *always* set to zero. This lets us avoid any if
        # statements when looping over neighbours, and we avoid any branch
        # prediction mishaps. Hell, it might not even be an optimization. But
        # if it turns out that it slows down the code, the only thing I need to
        # change is add an if-statement that checks for a zero when I'm looping
        # over the lists of neighbours
        #
        # My original implementation of this used scipy's sparse matrices to
        # store the adjacency matrix of the graph, but unfortunately numba
        # couldn't optimize that very well. This works on essentially the same
        # principle.
        self.vertices = np.empty(shape=n+1, dtype=int8)
        self.vertices[0] = 0

        for i in range(n):
            if sp.random.random() <= 0.5:
                self.vertices[i+1] = 1
            else:
                self.vertices[i+1] = -1

        num_edges = np.zeros(shape=n, dtype=int64)
        for i in range(edges.shape[0]):
            num_edges[edges[i,0]] += 1
        max_edges = num_edges.max()

        self.edges = np.zeros(shape=(n, max_edges), dtype=int64)
        for i in range(edges.shape[0]):
            for j in range(max_edges):
                if self.edges[edges[i,0]-1,j] == 0:
                    self.edges[edges[i,0]-1,j] = edges[i,1]
                    break

    def neighbours(self, i):
        return self.edges[i-1,:]

    def flip(self, i):
        self.vertices[i] *= -1

    def vertex(self, i):
        return self.vertices[i]


def toroidal_lattice(w, h):
    shape = (w * h, 4)
    neighbours = []
    for i in range(w * h):
        x = i % w
        y = i // w
        neighbours += [
            (i+1, ((x + 1) % w) + y*w + 1),
            (i+1, ((x - 1) % w) + y*w + 1),
            (i+1, x + ((y + 1) % h)*w + 1),
            (i+1, x + ((y - 1) % h)*w + 1)
        ]
    s = SpinGraph(w*h, np.array(neighbours))
    return s


def toroidal_octagonal_lattice(w, h):
    """Same as the toroidal lattice, but cells diagonal to eachother are
    neighbours"""
    shape = (w * h, 4)
    neighbours = []
    for i in range(w * h):
        x = i % w
        y = i // w
        neighbours += [
            (i+1, ((x + 1) % w) + y*w + 1),
            (i+1, ((x - 1) % w) + y*w + 1),
            (i+1, x + ((y + 1) % h)*w + 1),
            (i+1, x + ((y - 1) % h)*w + 1),
            (i+1, ((x + 1) % w) + ((y+1) % h)*w + 1),
            (i+1, ((x - 1) % w) + ((y+1) % h)*w + 1),
            (i+1, ((x + 1) % w) + ((y-1) % h)*w + 1),
            (i+1, ((x - 1) % w) + ((y-1) % h)*w + 1),
        ]
    s = SpinGraph(w*h, np.array(neighbours))
    return s


def hexagonal_lattice(w, h):
    shape = (w * h, 4)
    neighbours = []
    for i in range(w * h):
        x = i % w
        y = i // w
        if y % 2:
            neighbours += [
                (i+1, ((x + 1) % w) + ((y-1)%h)*w + 1),
                (i+1, (x % w) + ((y-1)%h)*w + 1),
                (i+1, ((x+1)%w) + y*w + 1),
                (i+1, ((x-1)%w) + y*w + 1),
                (i+1, ((x + 1) % w) + ((y+1)%h)*w + 1),
                (i+1, (x % w) + ((y+1)%h)*w + 1),
            ]
        else:
            neighbours += [
                (i+1, ((x - 1) % w) + ((y-1)%h)*w + 1),
                (i+1, (x % w) + ((y-1)%h)*w + 1),
                (i+1, ((x+1)%w) + y*w + 1),
                (i+1, ((x-1)%w) + y*w + 1),
                (i+1, ((x - 1) % w) + ((y+1)%h)*w + 1),
                (i+1, (x % w) + ((y+1)%h)*w + 1),
            ]
    s = SpinGraph(w*h, np.array(neighbours))
    return s


def linear_lattice(l):
    neighbours = []
    for i in range(l):
        neighbours += [
            (i+1, (i-1)%l + 1),
            (i+1, (i+1)%l + 1)
        ]
    return SpinGraph(l, np.array(neighbours))
